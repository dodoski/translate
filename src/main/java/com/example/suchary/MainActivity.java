package com.example.suchary;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void tossJoke(View view) {
        String[] jokeArr = new String[4];
        int some;
        Random gene = new Random();
        EditText text = findViewById(R.id.editText2);

        jokeArr[0] = getResources().getString(R.string.simple_string);
        jokeArr[1] = getResources().getString(R.string.dos);
        jokeArr[2] = getResources().getString(R.string.tres);
        jokeArr[3] = getResources().getString(R.string.quatro);

        for(int i = 0; i < 4; i++){
            some = gene.nextInt(4);
            text.setText(jokeArr[some]);
        }
    }
}
